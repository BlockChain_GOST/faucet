const Web3 = require('web3');

const config = require('./eth.json');

const web3 = new Web3();
web3.setProvider(new Web3.providers.HttpProvider(config.http_provider));

module.exports = web3;