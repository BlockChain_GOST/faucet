const argv = require('minimist')(process.argv.slice(2));

const app =  require('./server/app');

const config = require('./configs/config.json');

const PORT = argv.port || process.env.port || config.port;

const server = app.listen(PORT, () => {
    console.log('Server started at port', server.address().port);
});