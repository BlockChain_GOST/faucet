const web3 = require('../../configs/web3provider');

exports.getBlocks = async (maxBlockCount) => {
    return web3.eth
        .getBlockNumber()
        .then(latestBlock => {
            const blocks = [];
            let nextBlock = latestBlock;
            while (nextBlock >= 0 && blocks.length < maxBlockCount) {
                blocks.push(web3.eth.getBlock(nextBlock));
                nextBlock--;
            }
            return Promise.all(blocks);
        })
        .then(blocks => {
            return Promise.resolve(blocks);
        });
};

exports.getLatestBlock = () => {
    web3.eth.getBlockNumber().then(latestBlock => {
        return latestBlock;
    });
};