const request = require('request');
const config = require('../../configs/config.json');


exports.getProfile = function (access_token) {
    return new Promise((resolve, reject) => {
        request(config.profile_url + '?access_token=' + access_token,
            (err, resp, body) => {
                const error = err || resp.statusCode !== 200 ? resp.statusCode : false;
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(resp.body));
                }
            });
    });
};

exports.propertyAdd = function (access_token, type, value) {
    return new Promise((resolve, reject) => {
        var options = {
            method: 'POST',
            url: config.property_url,
            qs: { access_token: access_token, value: value, type: type },
        };

        request(options, function (error, response, body) {
            const err = error || response.statusCode !== 200;
            if (err) {
                reject(err);
            } else {
                resolve(true);
            }
        });

    });
};