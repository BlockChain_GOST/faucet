
exports.sessionRemove = function (req, res, redirectUri) {
    req.session.destroy(() => {
        delete req.session;
        res.redirect(redirectUri || '/');
    });
};