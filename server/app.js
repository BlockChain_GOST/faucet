const express = require('express');
const app = express();

const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');

const routes =  require('./routes');

app.set('views', path.join(__dirname, '/../views'));
app.set('view engine', 'pug');

app.use(express.static(__dirname + '/../public'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cookieParser());

app.use(session({
    resave: false,
    saveUninitialized: true,
    secret: 'top-kek'
}));

app.use('/', routes);

// app.use((req, res, next) => {
//     const err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// app.use((err, req, res, next) => { 
//     res.sendStatus(err.status || 500);
// });

module.exports = app;