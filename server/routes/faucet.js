const express = require('express');

const router = express.Router();

const web3 = require('../../configs/web3provider');

const trustednet = require('../service/trustednet');
const helpers = require('../service/helpers');

async function getAddress(access_token) {
    const profile = await trustednet.getProfile(access_token);
    return profile.user.properties.filter((item) => item.type === 'eth_address')[0];
}


router.get('/address/list', async (req, res) => {
    if (!req.session.authenticated) {
        res.sendStatus(401);
    } else {
        try {
            const access_token = req.session.access_token;
            const address = await getAddress(access_token);
            if (!!address && address.value) {
                res.json(address);
            } else {
                res.json(null);
            }

        } catch (error) {
            if (error === 401) {
                helpers.sessionRemove(req, res, '/#faucet');
            } else {
                res.sendStatus(500);
            }
        }
    }
});

router.post('/address/add', async (req, res) => {
    if (!req.session.authenticated) {
        res.sendStatus(401);
    } else {
        var newAddress = req.body.address;
        if (!newAddress) {
            res.sendStatus(400);
        } else {
            if (!web3.utils.isAddress(newAddress)) {
                res.status(400).send({ error: 'Не является адресом' });
            } else {
                try {
                    const access_token = req.session.access_token;
                    const addressIsAdded = await trustednet.propertyAdd(access_token, 'eth_address', newAddress);
                    res.status(200);
                    res.json({ addressIsAdded });
                    // TODO
                } catch (error) {
                    if (error === 401) {
                        helpers.sessionRemove(req, res, '/#faucet');
                    } else {
                        res.sendStatus(500);
                    }
                }
            }
        }
    }
});

router.post('/address/remove', async (req, res) => {
    if (!req.session.authenticated) {
        res.sendStatus(401);
    } else {
        try {
            const access_token = req.session.access_token;
            const addressIsAdded = await trustednet.propertyAdd(access_token, 'eth_address', null);
            res.json({ status: 200, success: addressIsAdded });
        } catch (error) {
            if (error === 401) {
                helpers.sessionRemove(req, res, '/#faucet');
            } else {
                res.sendStatus(500);
            }
        }
    }
});


router.get('/coins/get', async (req, res) => {
    if (!req.session.authenticated) {
        res.sendStatus(401);
    } else {
        const address = req.query.address;
        if (address) {

            if (!web3.utils.isAddress(address)) {
                res.sendStatus(400);
            } else {
                try {

                    web3.eth.sendTransaction({
                        from: '0xb471c2d5241099160c8dbb70c8d81f8ffce56fe2',
                        to: address,
                        value: web3.utils.toWei('1', 'ether')
                    }, (err) => {
                        if (err) {
                            res.json({ success: false, status: 500, err: err });
                        } else {
                            res.json({ success: true, status: 200 });
                        }
                    });
                } catch (err) {
                    res.sendStatus(500);
                }
            }
        } else {
            res.sendStatus(400);
        }

    }
});

module.exports = router;