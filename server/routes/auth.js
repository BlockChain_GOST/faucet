const express = require('express');
const request = require('request');

const router = express.Router();

const config = require('../../configs/config.json');
const trustednet = require('../service/trustednet');
const helpers = require('../service/helpers');

router.get('/', (req, res) => {
    const redirectUri = config.host_url + '/auth/callback';
    if (!req.session.authenticated) {
        res.redirect(config.auth_url + '?JSESSIONID=&response_type=code&redirect_uri=' + redirectUri + '&client_id=' + config.client_id);
    } else {
        res.redirect(config.auth_url + '?response_type=code&redirect_uri=' + redirectUri + '&client_id=' + config.client_id);
    }
});


router.get('/callback', (req, res) => {
    request.post({
        url: config.token_url,
        form: {
            code: req.query.code,
            grant_type: 'authorization_code',
            redirect_uri: config.host_url + '/auth/callback'
        },
        auth: {
            user: config.client_id,
            pass: config.client_secret,
            sendImmediately: false
        }
    }, async (err, resp, body) => {
        req.session.authenticated = true;
        req.session.access_token = JSON.parse(body).access_token;
        try {
            const profile = await trustednet.getProfile(req.session.access_token);
            req.session.user = profile.data;
            res.redirect('/');
        } catch (err) {
            res.redirect('/');
        }
    });
});

router.get('/logout', (req, res) => {
    request(config.auth_url, () => {
        helpers.sessionRemove(req, res, '/');
    });
});

module.exports = router;