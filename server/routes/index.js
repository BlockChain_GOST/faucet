const express = require('express');

const authRouter = require('./auth');
const faucetRouter = require('./faucet');

const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { authenticated: !!req.session.authenticated });
});

router.use('/auth', authRouter);
router.use('/faucet', faucetRouter);

module.exports = router;