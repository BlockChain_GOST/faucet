if (typeof window.location.origin === 'undefined') {
    window.location.origin = window.location.protocol + '//' + window.location.host;
}

var ANIMATION_SPEED = 200;
var latestBlocks = [];

var api = {
    faucet: {
        getAddresses: function (cb) {
            $.ajax({
                url: '/faucet/address/list',
                success: function (data) {
                    cb(undefined, data);
                },
                error: function (err) {
                    cb(err);
                }
            });
        },
        addAddress: function (address, cb) {
            $.ajax({
                url: '/faucet/address/add',
                type: 'POST',
                data: { address },
                success: function () {
                    cb(undefined, true);
                },
                error: function (err) {
                    cb(err);
                }
            });
        },
        removeAddress: function (address, cb) {
            $.ajax({
                url: '/faucet/address/remove?address' + address,
                type: 'POST',
                success: function (data) {
                    cb(undefined, data);
                },
                error: function (err) {
                    cb(err);
                }
            });
        },
        getEth: function (address, cb) {
            $.ajax({
                url: '/faucet/coins/get',
                type: 'GET',
                data: { address },
                success: function (data) {
                    cb(undefined, data);
                },
                error: function (err) {
                    cb(err);
                }
            });
        }
    }
};

var timeStampToDate = function (timestamp) {
    var newDate = new Date();
    newDate.setTime(timestamp * 1000);
    return newDate;
};

var renderAddress = function () {
    api.faucet.getAddresses(function (err, data) {
        if (err) {
            console.warn(err.statusText);
        } else {
            $('#addresses-list').empty();
            var addresses = data && data.value.split(' ');
            if (addresses && addresses.length) {
                $('.fixed-action-btn').fadeOut(ANIMATION_SPEED);
                for (var key in addresses) {
                    //
                    $('#addresses-list').append(`
                        <li class="collection-item address-item"'>
                            <div class="grey-text text-darken-4">${addresses[key]}
                                <div onclick='getEth(event, "${addresses[key]}")' class="secondary-content blue-text">
                                    <i class="material-icons">send</i>
                                </div>
                                <div onclick='removeAddress(event, "${addresses[key]}")' class="secondary-content red-text">
                                    <i class="material-icons">delete</i>
                                </div>
                            </div>
                        </li>
                    `);
                }
            } else {
                $('.fixed-action-btn').fadeIn(ANIMATION_SPEED);
                $('#addresses-list').append(`
                    <a class="collection-item address-item">
                        <div class="grey-text text-darken-4">Добавьте адрес кошелька</div>
                    </a>
                `);
            }
        }
    });
};

var addAddress = function (address) {
    api.faucet.addAddress(address, function (err, data) {
        if (err) {
            console.warn(err.statusText);
            if (err.responseJSON && err.responseJSON.error) {
                M.toast({ html: err.responseJSON.error });
            }
        } else {
            renderAddress();
        }
    });
};

var removeAddress = function (event, address) {
    event.preventDefault();
    event.stopPropagation();
    api.faucet.removeAddress(address, function (err, data) {
        if (err) {
            console.warn(err.statusText);
        } else {
            console.log(data);
            renderAddress();
        }

    });
};

var getEth = function (event, address) {
    event.preventDefault();
    event.stopPropagation();
    api.faucet.getEth(address, function (err, data) {
        if (err) {
            console.warn(err.statusText);
        } else {
            if (data.success) {
                M.toast({ html: 'Эфир успешно получен' });
            } else {
                M.toast({ html: 'Не удалось получить эфир' });
            }
        }
    });
};

var resize = function () {
    var hash = window.location.hash;
    var isElemExist = $(hash + '-frame-wrapper').length;

    if (isElemExist) {
        var limit = document.body.clientWidth;
        var scale = limit / 1920;

        $(hash + '-frame-wrapper').width(limit);
        $(hash + '-frame-wrapper').height(document.body.clientHeight / scale);
        $(hash + '-frame-wrapper').css({
            transform: 'scale(' + (scale) + ')',
            transformOrigin: '0 0'
        });
    }
};

(function () {

    // Init materialize
    $('.modal').modal();
    $('.sidenav').sidenav();

    $(window).resize(resize);
    $('#address-modal__cancel').click(function () {
        $('#adress_field').val('0x');
    });

    $('#address-modal__submit').click(function () {
        var address = $('#adress_field').val();
        if (!address || address == '0x') {
            console.log('Address must be not empty');
        } else {
            console.log(address);
            addAddress(address);

            $('#adress_field').val('0x');
        }
    });

    document.addEventListener('resize', resize);

    /*
    *   Init router
    */
    var utils = {
        renderPageTemplate: function (templateId) {
            setTimeout(function () {
                $('body').css({ overflowY: 'auto' });
                $(templateId).fadeIn(ANIMATION_SPEED);
            }, ANIMATION_SPEED);
        },
        renderFrameTemplate: function (hash) {
            setTimeout(function () {
                $('body').css({ overflowY: 'hidden' });
                $(hash + '-frame-wrapper').fadeIn(ANIMATION_SPEED);
                resize();
            }, ANIMATION_SPEED);
        }
    };

    var router = {
        routes: {},
        init: function () {
            console.log('router was created...');
            this.bindEvents();

            $(window).trigger('hashchange');
        },
        bindEvents: function () {
            $(window).on('hashchange', this.render.bind(this));
        },

        render: function () {
            var keyName = window.location.hash.split('/')[0];

            var url = window.location.hash;

            $('#netstat-frame-wrapper').fadeOut(ANIMATION_SPEED);
            $('#ide-frame-wrapper').fadeOut(ANIMATION_SPEED);
            $('#explorer-frame-wrapper').fadeOut(ANIMATION_SPEED);
            $('#faucet').fadeOut(ANIMATION_SPEED);
            $('#explorer').fadeOut(ANIMATION_SPEED);
            $('.fixed-action-btn').fadeOut(ANIMATION_SPEED);

            if (this.routes[keyName]) {
                this.routes[keyName](url);
            } else {
                var firstElem = Object.keys(this.routes)[0];
                this.routes[firstElem];
            }
        }
    };

    var routes = {
        '#netstat': function (url) {
            utils.renderFrameTemplate('#netstat');
        },
        '#ide': function (url) {
            utils.renderFrameTemplate('#ide');
        },
        '#faucet': function (url) {
            utils.renderPageTemplate('#faucet');
            renderAddress();
        },
        '#explorer': function (url) {
            utils.renderFrameTemplate('#explorer');
        }
    };

    var spaRouter = $.extend({}, router, {
        routes: routes
    });

    spaRouter.init();
    if (!window.location.hash) {
        window.location.hash = '#netstat';
    }
})();